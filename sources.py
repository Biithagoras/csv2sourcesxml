#!/bin/env python3
import csv
import xml.etree.ElementTree as ET
from datetime import datetime
digits={'0','1','2','3','4','5','6','7','8','9'}
def generate_xml_tag_text(tag,text):
	xml_new=ET.Element(tag)
	xml_new.text=text
	return xml_new

def trim(s,add_rm=set()):
	rm={","," ",";"}.union(add_rm)
	while s[-1:] in rm:
		s=s[:-1]
	while s[:1] in rm:
		s=s[1:]
	return s

def get_sourcetype(s):
	sourcetype={
		"Buch":"Book",
		"Book":"Book",
		"buch":"Book",
		"Artikel":"ArticleInAPeriodical",
		"ArticleInAPeriodical":"ArticleInAPeriodical",
		"Sammelband":"BookSection",
		"BookSection":"BookSection",
		"Unveröffentlichte Arbeit":"Book",
		"Internet":"InternetSite",
		"InternetSite":"InternetSite",
		"Misc":"Misc",
	}
	if s not in sourcetype.keys():
		print("Unknown SourceType:",s)
		return s
	else:
		return sourcetype[s]

def get_PeriodicalTitle(PTs):
	PTs=trim(PTs)
	if PTs[-1]==')' and '(' in PTs:
		I=PTs[PTs.index("(")+1:PTs.index(")")]
		PT=trim(PTs[:PTs.index("(")])
		
		sep=", "
		if not sep in PTs: #then a space will be used for seperating.
			sep=" "
		if not sep in PTs:
			print("ERROR: Only I but no V in",PTs)
			return [PTs]
		i=0 #position of last seperator
		while sep in PT[i:]:
			i+=PT[i:].index(sep)+len(sep)
		V=PT[i:]
		PT=PT[:i-len(sep)]
		return [PT,V,I]
	elif PTs[-1] in digits:#only one volume
		i=1
		while PTs[-(i+1)] in digits:#find last digit
			i+=1
		return [PTs[:-i],PTs[-i:]]
	else:
		print("Warning:",PTs,"is not PTs.")
		#print("PTs[-1] =",PTs[-1])
		return [PTs]


def part_person(person_str):
	pers=dict()
	person_l=trim(person_str).split(sep=",")#Todo, remove nonsense parts
	pers["c"]=min([3,len(person_l)])
	pers["Last"]=trim(person_l[0])
	if len(pers["Last"])<2:
		print("Waring: Lastname is short. It has only",len(pers["Last"]),"Chars,")
		print("Lastname:",pers["Last"])
		print("person_str:",person_str)
	if pers["c"]>1:
		pers["First"]=trim(person_l[-1])
	if pers["c"]>2:
		pers["Middle"]=trim("".join(person_l[1:-1]))
	return pers

def get_pages(p):
	p=trim(p,add_rm={"S",".","s"})
	p=p.replace("–","-")
	for i in p:
		if i not in digits.union({"-"}):
			print("Warning:",p,"is not formated as pages")
			#print("==DEBUG==")
			#print("Bad Pagechar:",p,i)
			#print("ord =",ord(i))
			#print("=========\n")
	return p

def get_list_author_str(author_str):
	persons=[]
	for person_str in author_str.replace(' und ',';').replace(' and ',';').replace('&',';').split(sep=";"):
		if len(person_str)>0:
			persons.append(part_person(person_str))
			if len(person_str)<2:
				print("Warining, Person is short:",person_str)
				print("From:",author_str)
	return persons

def generate_xml_author(tag,author_str,hrsg=False):
	l=get_list_author_str(author_str)
	xml_namelist=ET.Element("NameList")
	lc=0
	for p in l:
		lc+=1
		xml_pers=ET.Element("Person")
		if lc==len(l) and hrsg:
			p["Last"]+=" (Hrsg.)"
		xml_append(xml_pers,"Last",p["Last"])
		if p["c"]>=2:
			xml_append(xml_pers,"First",p["First"])
			if p["c"]>=3:
				xml_append(xml_pers,"Middle",p["Middle"])
		xml_namelist.append(xml_pers)
	xml_schachtel1=ET.Element(tag)
	xml_schachtel1.append(xml_namelist)
	return xml_schachtel1

def get_xml_from_row(row,head):
	source=ET.Element("Source")
	ID=row[head.index('Nummer')]
	SourceType=get_sourcetype(row[head.index('Typ')])
	Title=row[head.index('Titel')]
	Guid=chr(0x7b)+str(ID)+chr(0x7d)
	Author=generate_xml_author("Author",author_str=row[head.index('Autor')])
	Year=row[head.index('Jahr')]
	Tag=row[head.index('Autor')][:3]+Year[2:]
	#Tag=str(ID)
	City=row[head.index('Erscheinungsort')]
	
	xml_append(source,"Tag",Tag)
	xml_append(source,"SourceType",SourceType)
	xml_append(source,"Guid",Guid)
	xml_append(source,"Title",Title)
	xml_append(source,"Year",Year)
	xml_append(source,"City",City)
	source_author=ET.Element("Author")
	source.append(source_author)
	source_author.append(Author)
	
	if SourceType=="Book" or SourceType=="Misc":
		Publisher=row[head.index('Zeitschrift/Verlag')]
		xml_append(source,"Publisher",Publisher)
	elif SourceType=="BookSection":
		BookTitle=row[head.index('Sammelband Titel')]
		xml_append(source,"BookTitle",BookTitle)
		BookAuthor_str=author_str=row[head.index('Sammelband Herausgeber')]
		BookAuthor_str=BookAuthor_str.replace("(Hrsg.)","").replace("Hrsg.","").replace("(Hrsg)","").replace("Hrsg","")
		BookAuthor=generate_xml_author("BookAuthor",BookAuthor_str,hrsg=True)
		source_author.append(BookAuthor)
		Publisher=row[head.index('Zeitschrift/Verlag')]
		xml_append(source,"Publisher",Publisher)
		Pages=get_pages(row[head.index('Seitenbereich')])
		trim(row[head.index('Seitenbereich')],add_rm={"S",".","s"})
		xml_append(source,"Pages",Pages)
	elif SourceType=="ArticleInAPeriodical":
		PTL=get_PeriodicalTitle(row[head.index('Zeitschrift/Verlag')])
		if len(PTL)==1:
			xml_append(source,"PeriodicalTitle",PTL[0])
		elif len(PTL)==3:
			xml_append(source,"PeriodicalTitle",PTL[0])
			xml_append(source,"Volume",PTL[1])
			xml_append(source,"Issue",PTL[2])
		elif len(PTL)==2:
			xml_append(source,"PeriodicalTitle",PTL[0])
			xml_append(source,"Volume",PTL[1])
		else:
			print("ERROR: in get_PeriodicalTitle")
			print(PTL,"has not 1 nor 3 elements but",len(PTL))
		
		Pages=get_pages(row[head.index('Seitenbereich')])
		xml_append(source,"Pages",Pages)
		
	elif SourceType=="InternetSite":
		date_str=author_str=row[head.index('Datum')]
		date=datetime.strptime(date_str, '%d.%m.%Y')
		YearAccessed=date.year
		MonthAccessed=date.month
		DayAccessed=date.day
		InternetSiteTitle=row[head.index('Zeitschrift/Verlag')]
		URL=row[head.index('Internetlink')]
		xml_append(source,"YearAccessed",YearAccessed)
		xml_append(source,"MonthAccessed",MonthAccessed)
		xml_append(source,"DayAccessed",DayAccessed)
		xml_append(source,"InternetSiteTitle",InternetSiteTitle)
		xml_append(source,"URL",URL)
	return source

def xml_append(xml_root,tag,text):
	text=str(text)
	if len(text)<1:
		return xml_root
	xml_root.append(generate_xml_tag_text(tag,text))
	return xml_root

#export xml
tree = ET.parse('empty.xml')
root = tree.getroot()
with open('Data.csv', 'r') as csvfile:
	spamreader = csv.reader(csvfile)
	for head in spamreader:
		for i in range(len(head)):
			if "(" in head[i]:
				head[i]=trim(head[i][:head[i].index("(")])
		break
	for row in spamreader:
		xml_src=ET.Element("Source")
		root.append(get_xml_from_row(row,head))

xml_str=ET.tostring(root, encoding='unicode')
xml_str=xml_str.replace('\n',"")
xml_str=xml_str.replace('<','\n')
xml_str=xml_str.replace('\n/',"</b:")
xml_str=xml_str.replace('\n',"<b:")
xml_str=xml_str.replace("<b:Sources>",'<?xml version="1.0"?>\n<b:Sources SelectedStyle="" xmlns:b="http://schemas.openxmlformats.org/officeDocument/2006/bibliography" xmlns="http://schemas.openxmlformats.org/officeDocument/2006/bibliography">')
with open("Sources_output.xml", "w") as text_file:
    text_file.write(xml_str)
